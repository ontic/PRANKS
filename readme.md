# PRANKS

## Description
Parallel Ranking and Selection (PRANKS) is an algorithm designed for provably accurate, deterministic feature ranking on massive data sets.
The algorithm functions as follows: given an mxn matrix A, PRANKS computes a rank-revealing QR factorization of **_U^TA_**, or equivalently, **_SV^T_** where **_U^TAV=S_** is the singular value decomposition of **_A_**. This provides a feature ranking efficiently for massive tall and skinny matrices.

PRANKS is useful in scenarios where a fast, deterministic outcome is preferred over a slightly more accurate one with a risk of failure. This can happen in scenarios where it is important to keep models up to date as much as possible, which can be the case, for instance, of anomaly detection systems.

## Parameters
* k: the number of columns (features) to keep.
* Singular vectors: Since the matrix factorization is done on **_SV^T_** , the loadings on vectors corresponding to small singular values vanish and do not have a significant impact on the outcome of the algorithm. This parameter determines the number of singular vectors and values that will be taken into account. If the numerical rank of the input matrix is known to be less than *n*, lowering this parameter can make the algorithm faster with a negligible loss in accuracy.

## How to

### Platform
The algorithm has been tested on a cluster of 10 worker nodes and one master node running CentOS 6.4 and equipped with an Intel quad-core processor and 4 GB of RAM each. All machines were connected through an ethernet switch with a capacity of 100 Mbps on each link. The implementation was done using Scala 2.10.4 on Spark 1.4.1. The HDFS distributed file system implemented on Hadoop 2.6 was employed for data storage and access. 

### Input file format
The expected input file is an mxn numeric matrix. Each of the m lines represents a matrix row, and consists of a string of n whitespace-separated numbers. For instance:

```0.4835231 0.3474168 0.9217592 0.6012819 ...```

```0.2328477 0.5248122 0.3980504 0.9192953 ...```

```0.2334433 0.5256567 0.3988392 0.9194879 ...```

### Compile and run
The project uses Maven to compile & build. To download the dependencies and create the required jar file in the `target` directory, type the following command from the main directory of the project:

```
mvn package
```

This will generate a new directory called target containing an executable jar file named `pranks-1.0.jar`. This jar is actually a Spark executable with all the necessary dependencies.

To run the algorithm:

```spark-submit target/pranks-1.0.jar –-file=<input-file> --k=<integer> [--mp=<minPartitions> --sv=<svectors> --svd=<TRUE|FALSE> --std=<TRUE|FALSE>]```

```input-file: The relative path to the input file.```

```k: the number of columns (features) to keep.```

```minPartitions: The minimum number of partitions for the input file, as specified by Spark. ```

```sv: The number of singular vectors and values to consider. Default: k.```

```svd: whether or not to compute the SVD to compare the obtained residual Frobenius norm to that of the best rank-k approximation. If TRUE, said norm will be also written to the output. Default: FALSE.```

```std: whether or not to standardize the data to zero mean and unit variance. Default: TRUE.```


The value of minPartitions can have a dramatic impact on performance. We recommend 2 per available core.

## Examples
A toy data set is available in `example/toy_data`. It consists of a 10x10 matrix with three sets of numerically dependent columns (linear combinations with additive Gaussian noise). Below is an example of an execution keeping 3 columns, computing the SVD, standardizing the data and requesting 4 partitions.

```spark-submit target/pranks-1.0.jar --file="data/toy_data" --k=3 –svd=TRUE --mp=4```

## Results and output file format
The algorithm uses SLF4J for logging and output, and can therefore work with any user-specified log4j properties file. An example log4j.properties is provided, however, with the following configuration.
* All output is redirected to different files in a folder called logs, which will be created at the time of the execution in the directory from which the Spark job was invoked.
* All output above the WARN level is written to logs/err.out
* All other output above the DEBUG level is written to logs/info.out
* All other output above the TRACE level is written to logs/trace.out

The results of the algorithm are written to logs/trace.out. The contents of this file can be described as follows:
* A list of the input parameters
* A list of the Spark configuration variables
* Information on the input matrix dimensions and other parameters, execution modes and partial execution times.
* The total execution time in seconds (Algorithm time: x).
* The selected feature subset (Chosen subset).
* The Frobenius norm of the residual matrix (Residual frobenius norm).
* If applicable, the Frobenius norm of the residual matrix obtained with the best rank-k approximation (SVD).

