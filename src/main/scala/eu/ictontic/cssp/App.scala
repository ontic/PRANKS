package eu.ictontic.cssp

import org.apache.spark.{SparkContext, SparkConf}
import org.apache.spark.rdd.RDD
import org.apache.spark.mllib.{linalg => sla}
import org.apache.spark.mllib.linalg.{Matrix, Matrices, Vectors, DenseMatrix, DenseVector}
import org.apache.spark.mllib.linalg.distributed.{RowMatrix, MatrixEntry, CoordinateMatrix}
import breeze.{linalg => bla}
import breeze.numerics._
import breeze.linalg.qrp.QRP
import org.apache.spark.storage.StorageLevel
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import java.text.SimpleDateFormat;
import java.util.Date;

/**
  * @author Bruno Ordozgoiti
  */
object App {

  val mandatoryArgs = List("k", "file")
  val optionalArgs = List(
    ("mp", "2"), 
    ("sv", "-1"),
    ("svd", "FALSE"), 
    ("std", "TRUE")
  ).toMap
 
  def main(args : Array[String]) {

    //Parse the arguments
    val argPairs = optionalArgs ++ args.map(x => {
      val p = (x.tail tail) split "="      
      (p(0), p(1))
    }).toMap

    //Check that arguments are correct. Otherwise exit
    if ( mandatoryArgs.filter(argPairs.map(_._1).toList.contains(_)).length < mandatoryArgs.length || 
      !(argPairs.map(_._1).toList.filter(x => !(mandatoryArgs.contains(x) || optionalArgs.map(_._1).toList.contains(x))).isEmpty) ){
      Helper.printUsage
      return
    }

    val logger = LoggerFactory.getLogger("csspLogger")
    logger.trace("########################################")
    logger.trace("ARGS")
    argPairs.map(x => logger.trace(x._1 + " = " + x._2))
    logger.trace("########################################")

    val file = argPairs("file")
    val K = argPairs("k").toInt
    val minPartitions = argPairs("mp").toInt
    val SV = argPairs("sv").toInt
    val DO_SVD = argPairs("svd")
    val DO_STD = argPairs("std")

    val conf = new SparkConf().setAppName("Twostage_scala")
    val sc = new SparkContext(conf)

    logger.trace("**************************************************");
    logger.trace("PRANKS");
    logger.trace("12345 => 42351");
    logger.trace(":D");
    logger.trace("Starting job. Configuration:");
    for(t <- sc.getConf.getAll){
      logger.trace(t._1 + ": " + t._2);
    }
    logger.trace("**************************************************");


    //*********************************
    // Data input and preprocessing
    //*********************************
    val readStart = System.currentTimeMillis
    val distFile = sc.textFile(file, minPartitions)
    val readEnd = System.currentTimeMillis
    logger.trace("Input read time: " + (readEnd-readStart)/1000.0)

    val prerows = distFile.map(s => {
      val row = s.split(" ")
      Vectors.dense(row.map(_.toDouble))
    }).persist(StorageLevel.MEMORY_ONLY)
  
    val M = prerows.count
    val N = prerows.first.size
    val FACTOR = math.log(K);
    val C = math.min(math.max(2, K*FACTOR), N).toInt    
    val SAMPLES = math.min((math.pow(2, math.sqrt(0.3*N-K))*2), 40).toInt

    logger.trace("M: " + M)
    logger.trace("N: " + N)
    logger.trace("K: " + K)
    logger.trace("C: " + C)
    logger.trace("Random samples: " + SAMPLES)

    val stdStart = System.currentTimeMillis

    //Standardize the data to zero mean and unit variance
    val rows = DO_STD match {
      case "TRUE" => {
        val colAvgs = prerows.map(x => new bla.DenseVector(x.toArray)).reduce((a,b) => a + b).map(_ / M)
        val bcColAvgs = sc.broadcast(colAvgs)
        val colSds = prerows.map(x => {
          val avgs = bcColAvgs.value
          val v = new bla.DenseVector(x.toArray) - avgs
          (v :* v)
        }).reduce((a,b) => a + b).map(x => if (x == 0.0) 1 else math.sqrt(x/(M-1)))
        val bcColSds = sc.broadcast(colSds)
        
        prerows.map(x => {
          val sds = bcColSds.value
          val avgs = bcColAvgs.value
          val v = (new bla.DenseVector(x.toArray) - avgs) :/ sds
          //new org.apache.spark.mllib.linalg.DenseVector(v.toArray)
          Vectors.dense(v.toArray)
        }).persist(StorageLevel.MEMORY_ONLY)        
      }
      case "FALSE" => prerows    
    }

    val stdEnd = System.currentTimeMillis
    logger.trace("Standardization time: " + (stdEnd-stdStart)/1000.0)

    if (DO_STD == "FALSE"){
      prerows.unpersist(false)
    }

    //*********************************
    //*** The algorithm starts here ***
    val start = System.currentTimeMillis

    val sVectors = math.min(N,math.max(K, SV))

    //Compute the SVD
    val usv = new RowMatrix(rows).computeSVD(sVectors, false, 1e-9)
    val singularVectors = new sla.DenseMatrix(usv.V.numRows, usv.V.numCols, usv.V.toArray)
    val vT = singularVectors.transpose    
    val singularValues = new sla.DenseVector(usv.s.toArray)
    val compRank = singularValues.toArray.filter(_ > 1e-2).length
    logger.trace("Numerical rank at least " + compRank)

    val indices = Range(0, N)
    val vArray = singularVectors.toArray
    //Compute the product of Sigma and V transposed
    val SVArray = Range(0, sVectors).map(x => Range(0, N).map(y => singularVectors.transpose(x, y) * math.pow(singularValues(x), 1) )).flatten.toArray
    val fullSigmaVT = new bla.DenseMatrix(sVectors, N, SVArray)
    
    //Scale SigmaVT and do the RRQR, keeping the first k columns
    val sampledArray = Range(0,N).map(x => Range(0,K).map(y => vT.apply(y,x) * math.pow(singularValues(y), 1))).flatten.toArray
    val dm = new bla.DenseMatrix(K, N, sampledArray)
    val QRP(_QQ, _RR, _P, _pvt) = bla.qrp(dm)
    val chosenColumns = _pvt.take(K).toList.sorted

    val end = System.currentTimeMillis

    //************************************************************
    //Compute the norm of the residual matrix to evaluate the result
    
    //Compute (C^T C)^-1 C^T A (regressionMatrix)
    val bcChosen = sc.broadcast(chosenColumns)
    val ctA = rows.map(row => {
      val chosen = bcChosen.value
      val cRow = row.toArray.zipWithIndex.filter(chosen contains _._2).map(_._1)
      val v1 = new bla.DenseMatrix(cRow.size, 1, cRow.toArray)
      val v2 = new bla.DenseMatrix(1, row.size, row.toArray)
      v1 * v2
    }).reduce((x,y) => x + y)
    val ctc = ctA(::, chosenColumns.toList.sorted).toDenseMatrix
    val r = bla.rank(ctc)
    val ctci = bla.inv(ctc)
    val regressionMatrix = ctci * ctA

    //Finally, compute the norm
    val bcRegressionMatrix = sc.broadcast(regressionMatrix)
    val residualNorm = rows.map(row => {
      val regressionMatrix = bcRegressionMatrix.value
      val chosen = bcChosen.value
      
      val aRow = new bla.DenseVector(row.toArray)
      val cRow = aRow.toArray.zipWithIndex.filter(y => chosen contains y._2).map(y => y._1)
      val cRowM = new bla.DenseMatrix(1, cRow.size, cRow.toArray)
      val cpa = regressionMatrix
      val approx = cRowM * cpa
      val d = (new bla.DenseVector(approx.toArray) - aRow)
        (d :* d).sum
    }).reduce((a,b) => a+b)

    logger.trace("Algorithm time: " + (end-start)/1000.0)
    logger.trace("Chosen subset: " + chosenColumns)
    logger.trace("Residual frobenius norm: " + math.sqrt(residualNorm))

    if(DO_SVD == "TRUE"){
      val svdStart = System.currentTimeMillis
      val dataPairs = rows.zipWithIndex.map(x => (x._2, x._1.toDense))
      //Rebuild the matrix with SVD to obtain the lower bound to the
      //residual norm
      val rmData = new RowMatrix(rows)
      //Recompute the SVD to obtain U
      val svdF = rmData.computeSVD(K, true, 1e-9)
      val sigmaVT = new DenseMatrix(svdF.s.size, N.toInt, svdF.V.transpose.toArray.zipWithIndex.map(x => x._1 * svdF.s((x._2 % svdF.s.size).toInt)))
      val aPrime = svdF.U.multiply(sigmaVT)
      val apPairs = aPrime.rows.zipWithIndex.map(x => (x._2, x._1.toDense))//rows.map(x => (x.index, x.vector.toDense))
      val apDataPairs = apPairs.join(dataPairs).values
      val residualNormSVD = math.sqrt(
        apDataPairs.map(rows => {
          val cRow = new bla.DenseVector(rows._1.toArray)
          val aRow = new bla.DenseVector(rows._2.toArray)
          val d = (cRow - aRow)
            (d :* d).sum
        }).reduce((x,y) => x+y)
      )
      val svdEnd = System.currentTimeMillis
      logger.trace("SVD residual time: " + (svdEnd-svdStart)/1000.0)
      logger.trace("Residual frobenius norm with SVD: " + residualNormSVD)
    }

   sc.stop

  }

}

object Helper {
  val logger = LoggerFactory.getLogger("csspLogger")
  def printUsage(){
    val s =     "\r\n\r\n" +
    "----------\r\n" +
    "PRANKS: Parallel Ranking and Selection." +
    "\r\n\r\n" +
    "Usage: spark-submit <jar-file>.jar --file=<input file> --k=<number of chosen columns> [--mp=<minpartitions> --svd=TRUE|FALSE --std=TRUE|FALSE]" +
    "\r\n\r\n" +
    "--mp=<Integer>\n" + 
    "\t The value of minPartitions for the input file.\n" + 
    "--svd=TRUE|FALSE\n" +
    "\t If TRUE, compute the residual for the best rank-k approximation at the end of the algorithm (default FALSE).\n" + 
    "--std=TRUE|FALSE]\n" +
    "\t If TRUE, standardize the data to zero mean and unit variance (default TRUE).\n"

    println(s)
    logger.trace(s)
  }

  def toBreezeDenseMatrix(v : org.apache.spark.mllib.linalg.DenseMatrix) : bla.DenseMatrix[Double] = {
    return new bla.DenseMatrix[Double](v.numRows, v.numCols, v.toArray)
  }

}
